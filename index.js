const WebSocket = new require('ws');
const uniqid = require('uniqid');
const express = require('express');
const path = require('path');
const http = require('http');
const app = express();

const server = http.createServer(app);

const webSocketServer = new WebSocket.Server({ server });

const clients = {};
let chat = [];

webSocketServer.on('connection', function(ws) {  
  const id = uniqid();
  clients[id] = ws;

  if(chat.length) {
    clients[id].send(JSON.stringify(chat));
  }

  ws.on('message', function(message) { 
    chat = [...chat, message];
    for (var key in clients) {
      clients[key].send(JSON.stringify(message));
    }    
  });
  
  ws.on('close', function() {
    delete clients[id];
  });
});

if(process.env.NODE_ENV === 'production') { 
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(
      path.resolve(
        __dirname, 'client', 'build', 'index.html'
      )
    )
  })
}

server.listen(process.env.PORT || 8080, () => {
  console.log(`Server started on port`);
});