import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Navigation extends Component {
  render() {
    return (
      <nav>
        <a href="" className="logotype">
          Логотип
        </a>
        <ul className="navigation__list">
          <li className="navigation__item">
            <NavLink to="/" className="navigation__link">Главная</NavLink>
          </li>
          <li className="navigation__item">
            <NavLink to="/favorites" className="navigation__link">Избранное</NavLink>
          </li>
        </ul>
      </nav>
    )
  }
}

export default Navigation;
