import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Switch, Route} from 'react-router-dom';
import Navigation from '../components/navigation';
import MainPage from './Main';
import Chat from './Chat';

class RootComponent extends Component {
  render() {
    return (
      <div className="app">
        <Switch>
          <Route exact path="/" component={MainPage}/>
          <Route exact path="/chat" component={Chat}/>
        </Switch>
      </div>
    );
  }
}

export default RootComponent;
