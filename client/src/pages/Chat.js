import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import RootRef from '@material-ui/core/RootRef';

const Container = {
  position: 'fixed',
  bottom: 0,
  left: 0
}

class Chat extends Component {
  constructor() {
    super();

    this.state = {
      value: '',
      socket: null,
      name: null,
      chat: []
    }

    this.ref = React.createRef();
  }
  componentDidMount() {
    if(!localStorage.getItem('name')) {
      this.props.history.push('/');
    }
    
    this.name = localStorage.getItem('name');
    this.socket = new WebSocket('wss://zdln.herokuapp.com');
  
    this.socket.onmessage = (e) => {    
      if(e.data !== '') {
        this.append(JSON.parse(e.data));
      }
    }

    this.socket.onopen = () => {
      console.log('ws connected')
    };
  }

  renderChat = (message, idx) => {
    return (
      <li key={idx}>{message}</li>
    )
  }

  takeValue = (evt) => {
    this.setState({
      value: evt.target.value
    })
  }

  send = () => {
    if(this.state.value.trim() !== '') {
      this.socket.send(this.name + ': '  + this.state.value);
      this.ref.current.querySelector('input').value = '';
      this.setState({
        value: ''
      })
    }
    return;    
  }

  append = (message) => {
    if(Array.isArray(message)) {
      return this.setState({
        chat: [
          ...this.state.chat,
          ...message
        ]
      })
    }
    this.setState({
      chat: [
        ...this.state.chat,
        message
      ]
    })
  }


  render() {
    const { chat } = this.state;
    return (
      <div>
        <section>
          <div className="chat-body">
            <h3>Чатец</h3>
            <ul>
              { chat.length ? chat.map((it, idx) => this.renderChat(it, idx)) : 
                <span>Сообщение нет</span>
              }
            </ul>
          </div>
        </section>
        <footer style={Container}>
          <RootRef rootRef={this.ref}>
            <TextField
                id="outlined-email-input"
                label="Сообщение твоё"
                type="text"
                name="email"
                margin="normal"
                onKeyUp={this.takeValue}
                variant="outlined"
              />
          </RootRef>
          <p>
            <Button className="btn--send" onClick={this.send} variant="outlined" color="primary" type="submit">
              Отправить
            </Button>
          </p>
        </footer>
      </div>
    )
  }
}

export default Chat;
