import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import RootRef from '@material-ui/core/RootRef';
class MainPage extends Component {
  constructor() {
    super();
    this.state = {
      value: ''
    }
    this.ref = React.createRef();
  }

  componentDidMount() {
    if (localStorage.getItem('name')) {
      this.props.history.push('/chat');
    }
  }

  takeValue = (evt) => {
    this.setState({value: evt.target.value})
  }

  sendNameToWs = (evt) => {
    evt.preventDefault();
    const name = this.state.value.trim() || null;

    if (name) {
      localStorage.setItem('name', name);
      this.props.history.push('/chat');
    }
  }

  render() {
    return (
      <div>
        <form onSubmit={this.sendNameToWs}>
          <RootRef rootRef={this.ref}>
            <TextField
              id="outlined-email-input"
              label="Имя твоё"
              type="text"
              name="email"
              margin="normal"
              onKeyUp={this.takeValue}
              variant="outlined"
            />
          </RootRef>
          <p>
            <Button variant="outlined" color="primary" type="submit">
              Primary
            </Button>
          </p>
        </form>
      </div>
    )
  }
}

export default MainPage;
